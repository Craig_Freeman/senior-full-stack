﻿using EmployeeManagement.Domain.Repositories;

namespace EmployeeManagement.Domain
{
    public class Employee
    {
        public readonly int Id;
        private readonly string _name;
        private readonly Contract _contract;

        public Employee(int id, string name, Contract contract)
        {
            Id = id;
            _name = name;
            _contract = contract;
        }

        public bool CanBeArchived()
        {
            return _contract.IsExpired();
        }

        public string Name() => _name;
    }
}
