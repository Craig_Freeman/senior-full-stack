﻿using EmployeeManagement.Domain;

namespace EmployeeManagement.Infrastructure
{
    internal class EmployeeData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EmployeeType Type { get; set; }
        public double Salary { get; set; }

        public Employee ToDomain(Contract contract)
        {
            return new Employee(Id, Name, contract);
        }
    }
}
