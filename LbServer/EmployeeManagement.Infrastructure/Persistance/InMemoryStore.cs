﻿using EmployeeManagement.Domain;
using EmployeeManagement.Infrastructure.Persistance;
using System;
using System.Collections.Generic;

namespace EmployeeManagement.Infrastructure
{
    internal static class InMemoryStore
    {
        public static IList<EmployeeData> Employees = new List<EmployeeData>
        {
            new EmployeeData
            {
                Id = 1,
                Name = "John",
                Type = EmployeeType.Director,
                Salary = 20000
            },
            new EmployeeData
            {
                Id = 2,
                Name = "McClane",
                Type = EmployeeType.Director,
                Salary = 20000
            },
            new EmployeeData
            {
                Id = 3,
                Name = "Tom",
                Type = EmployeeType.Manager,
                Salary = 15000
            },
            new EmployeeData
            {
                Id = 4,
                Name = "Hanks",
                Type = EmployeeType.Employee,
                Salary = 8000
            }
        };

        public static IList<ContractData> Contracts = new List<ContractData>
        {
            new ContractData
            {
                EmployeeId = 1,
                ExpirationDate = new DateTime(2022, 01, 01)
            },
            new ContractData
            {
                EmployeeId = 2,
                ExpirationDate = new DateTime(2022, 01, 01)
            },
            new ContractData
            {
                EmployeeId = 3,
                ExpirationDate = new DateTime(2024, 04, 10)
            },
            new ContractData
            {
                EmployeeId = 4,
                ExpirationDate = new DateTime(2020, 01, 01)
            }
        };
    }
}
