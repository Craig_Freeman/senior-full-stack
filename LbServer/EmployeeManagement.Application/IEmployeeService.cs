﻿using EmployeeManagement.Domain;
using System.Collections.Generic;

namespace EmployeeManagement.Application
{
    public interface IEmployeeService
    {
        void DeleteEmployee(int id);
        IEnumerable<Employee> GetEmployees();
    }
}