import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IEmployee } from '../../resources/IEmployee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnChanges {
  @Input() employee!: IEmployee;

  name = '';

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.name = this.employee.name;
  }
}
