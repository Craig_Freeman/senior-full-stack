import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from 'src/app/dependency-imports/material.module';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeesResolver } from './resolvers/employees.resolver';
import { EmployeeService } from './services/employee.service';
import { EmployeeServiceToken } from './services/IEmployeeService';
import { EmployeesEffects } from './store/employees.effects';
import { employeesReducer } from './store/employees.reducer';
import { EmployeeSelectors } from './store/employees.selectors';
import { EmployeeComponent } from './components/employee/employee.component';

@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeComponent
  ],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    MaterialModule,
    StoreModule.forFeature('employees', employeesReducer),
    EffectsModule.forFeature([EmployeesEffects])
  ],
  providers: [
    {
      provide: EmployeeServiceToken, useClass: EmployeeService
    },
    EmployeesResolver,
    EmployeeSelectors
  ]
})
export class EmployeesModule { }
