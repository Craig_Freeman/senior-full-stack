import { createAction, props } from "@ngrx/store";
import { IEmployee } from "../resources/IEmployee";

export const employeeActions = {
    requestEmployees: createAction('[Employees] Trigger Loading Employees'),
    requestEmployeesSuccess: createAction('[Employees] Loaded Employees', 
        props<{ employees: IEmployee[] }>()
    ),
    requestEmployeeDelete: createAction('[Employees] Employee Delete Request', props<{ id: number }>()),
    requestEmployeeDeleteSuccess: createAction('[Employees] Employee Deleted Success', props<{ id: number }>())
}