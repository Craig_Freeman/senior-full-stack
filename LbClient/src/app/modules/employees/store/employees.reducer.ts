import { createReducer, on } from "@ngrx/store";
import { IEmployee } from "../resources/IEmployee";
import { employeeActions } from "./employees.actions";

export interface IEmployeesState {
    employees: IEmployee[];
}

const initialState: IEmployeesState = {
    employees: []
}

export const employeesReducer = createReducer(
    initialState,
    on(employeeActions.requestEmployeesSuccess, (state, action) => ({
        ...state, employees: action.employees
    })),
    on(employeeActions.requestEmployeeDeleteSuccess, (state, action) => ({
        ...state, employees: state.employees.filter(e => e.id !== action.id)
    }))
);